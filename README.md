# GNOME Scores [![Build Status](https://travis-ci.org/azmeuk/gnome-scores.svg?branch=master)](https://travis-ci.org/azmeuk/gnome-scores) [![Build status](https://ci.appveyor.com/api/projects/status/a01dbblx70bnw67q?svg=true)](https://ci.appveyor.com/project/azmeuk/gnome-scores)

That would be awesome to have a piece of software able to display and play musical scores, and allow
musicians to play over a score in order to practice. This should not be a score editor (at least, in a first time).

This is a basic rust repository, so make sur you have `cargo` and run `cargo run` to try GNOME Scores.

## Cool features in a first time

- GNOME integration and [GNOME HIG](https://developer.gnome.org/hig/stable/) respect.
- MusicXML score catalog (Like GNOME Documents for instance).
- MIDI playback.
- Selective display (a whole score, a whole system or juste a staff).
- Guitar tabs.
- Some playback functions:
  - Metronome tick.
  - Play one ore several empty metronome bars before playback.
  - Slow and speed up the general tempo.
  - Select some bars and loop on it.
  - Progressively grow or decrease the tempo while the loop is playing.
  - Sound bank selection.
  - Mute or solo systems and staves.
  - Get an instrument on the background or the foreground, without completely mute it or solo it.

## And tomorrow?

- Import a music file matching the score, and play the music while the score is displayed.
- GuitarPro, NoteworthyComposer, Finale, Sibelius files support.
- Synchronise with some online services. Musescore?
- MacOS and Windows support?

## And the day after tomorrow?

- Handclap, or count with your voice to start the playback at the tempo you like.
- Score OCR to import pdfs and handwritten files.


# I have some questions

Ok, go on.

## Are you an official GNOME project?

Nope.

## Why rust?

Because it looks fun.

## Can I contribute?

Yeah of course.
